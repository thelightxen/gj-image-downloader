﻿// gj_download.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>
namespace fs = ::std::experimental::filesystem;

#include <urlmon.h>
#include <cstdio>
#include <iostream>
#include <string>
#include <locale>
#include <sstream>
#include <urlmon.h>
#pragma comment (lib, "urlmon.lib")

using namespace std;

int main()
{
	IStream* stream;

	string x;

	int i_input = 1;

	cout << "Select Method (press enter): \n m- Message Pictures. \n a- Avatars \n\n"; 

	cin >> x;

	if (x == "m" || x == "a" || x == "M" || x == "A")
	{
		cout << "Start at...\n\n";

		cin >> i_input;

		cout << "\n------------------------------------------------------------\n";
	}
	else 
	{

		cout << "Error :(";
	
		return -1;

	}

	if (x == "m" || x == "M") {

		fs::create_directory("Pictures");

		for (int i = i_input; i < 2147483647; ++i)
		{
			auto const index_img{ i };
			wstringstream ss_url, ss_file;

			ss_url << "https://m.gjcdn.net/content/1000/" << i << ".jpg";
			ss_file << "Pictures/" << i << ".jpg";

			wstring wURL = ss_url.str();
			LPWSTR sURL = const_cast<LPWSTR>(wURL.c_str());


			wstring wFILE = ss_file.str();
			LPWSTR dFile = const_cast<LPWSTR>(wFILE.c_str());

			if (S_OK == URLDownloadToFile(NULL, sURL, dFile, 0, NULL))
			{
				cout << "Saved " << i << ".jpg from " << "https://m.gjcdn.net/content/1000/" << i << ".jpg" << endl;
			}

		}

	}
	else if (x == "a" || x == "A")
	{
		
		fs::create_directory("Avatars");

		for (int i = i_input; i < 2147483647; ++i)
		{
			auto const index_img{ i };
			wstringstream ss_url, ss_file;

			ss_url << "https://m.gjcdn.net/user-avatar/1000/" << i << ".png";
			ss_file << "Avatars/" << i << ".png";

			wstring wURL = ss_url.str();
			LPWSTR sURL = const_cast<LPWSTR>(wURL.c_str());

			wstring wFILE = ss_file.str();
			LPWSTR dFile = const_cast<LPWSTR>(wFILE.c_str());

			if (S_OK == URLDownloadToFile(NULL, sURL, dFile, 0, NULL))
			{
				cout << "Saved " << i << ".png from " << "https://m.gjcdn.net/user-avatar/1000/" << i << ".png" << endl;
			}

		}

	}

	
}